// Allows us to use ES6 in our migrations and tests.
require('babel-register')

module.exports = {
  compilers: {
    solc: {
      version: "0.4.25"
    }
  },
  networks: {
    ganache: {
      host: '127.0.0.1',
      port: 7545,
      network_id: '*' // Match any network id
    }
  }
}
